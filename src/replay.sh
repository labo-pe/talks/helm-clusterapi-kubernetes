SCRIPT=$(readlink -f $0)
REPLAY_DIR=$(dirname $SCRIPT)/enregistrements


gnome-terminal --geometry=160x48 -- /usr/bin/scriptreplay -t $REPLAY_DIR/creation_cluster_k9s_timing.txt $REPLAY_DIR/creation_cluster_k9s

scriptreplay -t $REPLAY_DIR/creation_cluster_timing.txt $REPLAY_DIR/creation_cluster

read

scriptreplay -t $REPLAY_DIR/recupere_kubeconfig_timing.txt $REPLAY_DIR/recupere_kubeconfig