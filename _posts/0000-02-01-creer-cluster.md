## Créons un cluster

```shell
helm repo add cluster-api-vsphere https://labo-pe.gitlab.io/capv-helm-chart/
helm upgrade --install --namespace cluster-demo --create-namespace --values ./src/values.yaml --values ./src/secrets/values.yaml --version 4.1.0 cluster-demo cluster-api-vsphere/cluster-api-vsphere
```

```yaml
wait:
  apiServer: true
  timeout: 900

cluster:
  name: "cluster-demo"
  additionalClusterResourceSet:
    - kind: ConfigMap
      name: cni-calico

kubernetes:
  version: v1.23.5

machines:
  controlPlane:
    replicas: 3
    cpuCount: 2
    template: template-k8s-1-23-5-alma9-x86-64
    diskSizeGiB: 40
    memorySizeMiB: 8192
    nodeDrainTimeout: 20m
    machineHealthCheck:
      enabled: true

  workers:
    worker-md-0:
      replicas: 3
      cpuCount: 2
      template: template-k8s-1-23-5-alma9-x86-64
      diskSizeGiB: 40
      memorySizeMiB: 8192
      nodeDrainTimeout: 20m
      machineHealthCheck:
        enabled: true
        maxUnhealthy: 40%

cni:
  calico:
    enabled: true
    config: |
      calicoNetwork:
        nodeAddressAutodetectionV4:
          canReach: "10.209.16.1"
      imagePullSecrets: []
      kubernetesProvider: ""
```

Note:

Le chart en fin d'execution nous donne les commandes nécessaires pour récupérer la kubeconfig, nous l'utiliserons un peu plus tard

---

## Tour des objets

* Cluster: 
  * représente notre configuration globale du cluster, ses adresses réseau, ports etc
  * référence un ControlPlane

* ControlPlane (KubeadmControlPlane dans notre cas) :
  * Equivalent à un statefulset
  * Gère les noeuds du control plane

* MachineDeployment :
  * Equivalent à un deployment
  * Gère un MachineSet ( équivalent du ReplicaSet )
  * Gère les noeuds de type worker

* Machines et VSphereMachine :
  * Equivalent à des pods pour le MachineDeployment
  * Gèrent la création des VMs et leur configuration


Note:

Ces machines sont quasiment comme les pods, il est possible de les supprimer, elles seront supprimées de vsphere puis re-créées et rattachées au cluster

---

## Coup d'oeil au cluster généré

```bash
kubectl --namespace cluster-demo get secret cluster-demo-kubeconfig -o json | jq -r '.data.value' | base64 -d > ~/.kube/configs/config_demo
source ~/.profile
kctx cluster-demo-admin@cluster-demo
kubectl get nodes
kubectl -n kube-system get pods
```
