## Scale up

* Augmentons a 4 notre pool de workers :

```bash
helm upgrade --install \
  --namespace cluster-demo \
  --create-namespace \
  --values ./src/values.yaml \
  --values ./src/secrets/values.yaml \
  --set machines.workers.worker-md-0.replicas=4 \
  --version 4.1.0 \
  cluster-demo \
  cluster-api-vsphere/cluster-api-vsphere
```

Note:

On passe le pool de worker a 4 par le --set, ca aurait pu etre fait par les values.

--

## Scale down

* Réduisons a 2 workers mais 4 vCpus et 16Go de ram:

```bash
helm upgrade --install \
  --namespace cluster-demo \
  --create-namespace \
  --values ./src/values.yaml \
  --values ./src/secrets/values.yaml \
  --set machines.workers.worker-md-0.replicas=2 \
  --set machines.workers.worker-md-0.cpuCount=4 \
  --set machines.workers.worker-md-0.memorySizeMiB=16384 \
  --version 4.1.0 \
  cluster-demo \
  cluster-api-vsphere/cluster-api-vsphere
```

Note:

On passe encore par le --set, ca aurait pu etre fait par les values, besoin de la demos, toussa toussa.

Les paramètres CPU/Mémoire peuvent etre vus comme les limits cpu/mémoire d'un pod.
