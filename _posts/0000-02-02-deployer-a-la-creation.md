## Des déploiements a la création

```bash
kctx cluster-demo-admin@cluster-demo
kubectl -A get pods
```

![calico](images/deploiements_creation.png)

Note:

Calico ( notre cni ), kube-vip et vsphere-csi ne sont pas des déploiements "standard" d'un cluster kubernetes, ils ont été ajoutés par le chart

--

## Origine de ces déploiements

* Les ClusterResourceSet permettent la configuration initiale des déploiements du cluster

```yaml
cni:
  calico:
    enabled: true
[...]
capv:
  cluster:
    additionalClusterResourceSet:
      - kind: ConfigMap
        name: cni-calico
```

* Calico a été installé de cette façon
* Présente des limites actuellement
  * Permettent uniquement d'initialiser un cluster
  * La mise à jour n'est pas gérée
  * Uniquement des manifests yaml, pas de possibilité d'exécuter helm ou kustomize