## Pour aller plus loin

--

## Un cluster de management

* ClusterApi nous permet de pouvoir déplacer la gestion d'un cluster sur un autre cluster possédant ClusterApi. Grâce à cette capacité, nous possédons donc un cluster de management qui peut s'autogérer.

* Ce cluster de management est créé via l'utilisation du même chart helm que les autres clusters en utilisant kind sur une machine.

--

## Des backups de clusters

* Il est possible de créer des backups velero des clusters créé en ajoutant cette section au values du chart :
```yaml
velero:
  enabled: true
  schedule: "0 2 * * *"
  namespace: velero
  storageLocation: default
  ttl: 168h00m0s
```

* En cas de desastre et de perte du cluster de management, il est possible de recréer les clusters de management et de restaurer les backups des clusters afin d'en continuer la gestion.

--

## Cluster API Vsphere Provider Helm Chart

* Notre chart se sert massivement d'un chart opensource permettant de créer un cluster se basant sur Cluster API  Vsphere Provider : 
  * https://artifacthub.io/packages/helm/cluster-api-vsphere/cluster-api-vsphere
  * https://gitlab.com/labo-pe/capv-helm-chart/

--

## AutoScale d'un cluster


* L'outil ClusterAutoscaler permets d'automatiser le scale up/down en fonction de la charge des noeuds

Note:

De ce que l'on a pu voir :
  * permets de scale up un pool en se basant sur les ressources consommées
  * permets le scale down a 1 automatique
    * Ne permets pas encore le scale a 0 ( mais ça viendra )
  * Couplé aux HPAs on pourrait facilement imaginer un cluster qui suit la charge du workload

--

## Questions / Réponses