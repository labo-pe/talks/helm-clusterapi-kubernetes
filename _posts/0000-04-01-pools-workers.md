## Pools de workers

* Ajoutons un pool de workers disposant d'un label particulier

```bash
helm upgrade --install \
  --namespace cluster-demo \
  --create-namespace \
  --values sources/cluster-demo-values.yaml \
  --values sources/cluster-demo-pools.yaml \
  --values sources/secrets.yaml \
  --version 4.3.1 \
  cluster-demo pe/cluster-k8s-ipam
```

Note:

```bash
KUBECONFIG=~/.kube/configs/config_demo kubectl get nodes
KUBECONFIG=~/.kube/configs/config_demo kubectl get nodes -l pool=poolDeLuxe
KUBECONFIG=~/.kube/configs/config_demo kubectl get nodes -l pool=poolDeLuxe -o 'jsonpath={..capacity}'
```

Les valeurs vues par k9s en cpu/mémoire correspondent a l'enforcing, qui réserve des ressources au systeme et aux composants kubernetes